from django.urls import path
from . import views

appname = 'jadwal'

urlpatterns = [
    path('', views.jadwal_ku),
    path('<int:pk>/', views.delete, name = 'hapus'),
    
]